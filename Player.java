import java.util.*; //ArrayList
public class Player extends Person {
	private ArrayList<BJHand> splitHands; //Data structure to store hands to split
	private int bankroll; //Bankroll private to keep from alteration
	private int wager; //Wager private to keep from alteration after bet was made
	
	public Player() {
		initHands();
		setBankRoll(100); //Starting bankroll 
	}
	
	private void setBankRoll(int money) {
		bankroll = money;
	}
	
	private void setWager(int money) {
		wager = money;
	}
	
	private void initHands() {
		splitHands = new ArrayList<BJHand>();
	}
	
	public int getBankRoll() {
		return bankroll;
	}
	
	public int getWager() {
		return wager;
	}
	
	public ArrayList<BJHand> getHands() {
		return splitHands;
	}
	
	public void bet(int amount) {
		setBankRoll(this.getBankRoll() - amount);
		setWager(amount);
	}
	
	public void doubleDown() {
		setBankRoll(this.getBankRoll() - this.getWager());
		setWager(this.getWager()*2);
	}
	
	public void win() {
		setBankRoll(this.getBankRoll() + (2*this.getWager()));
		setWager(0);
	}
	
	public void lose() {
		setWager(0);
	}
	
	public void push() {
		setBankRoll(this.getBankRoll() + this.getWager());
		setWager(0);
	}
}
