public class Card {
	private String value;
	private String suit;
	private int numVal;

	public Card( String v, String s ) {
	  	this.setValue(v);
		this.setNumVal(v);
		this.setSuit(s);
	}

	private void setValue(String v) {
		this.value = new String(v);
	}

	private void setSuit(String s) {
		this.suit = new String(s);
	}

	private void setNumVal(String v) {
		if(v.equals("Ace")) {
			numVal = 1;
		}

		if(v.equals("King") || v.equals("Queen") || v.equals("Jack") ||
				v.equals("Ten")) {
			numVal = 10;
		}

		if(v.equals("Nine")){
			numVal = 9;
		}

		if(v.equals("Eight")){
			numVal = 8;
		}

		if(v.equals("Seven")){
			numVal = 7;
		}

		if(v.equals("Six")){
			numVal = 6;
		}

		if(v.equals("Five")){
			numVal = 5;
		}

		if(v.equals("Four")){
			numVal = 4;
		}

		if(v.equals("Three")){
			numVal = 3;
		}

		if(v.equals("Two")){
			numVal = 2;
		}
	}

	public String getVal() {
		return value;
	}

	public String getSuit() {
		return suit;
	}

	public int getNumVal() {
		return numVal;
	}

/*
	public boolean isSame(Card c) {
		if(c.getVal().equals(this.getVal())) {
			return true;
		} 
		return false;
	}

	//Only checks value and not suit for blackjack
	@Override	
	public boolean equals(Object o) {
		if( !(o instanceof Card) )
			return false;
	
		Card c = (Card)(o);
		if( c.toString().equals(this.toString()) )
			return true;
		else
			return false;
	}
*/
	//Returns number value for 2-10. returns first letter for Ace, King, Queen,
	// Jack.
	@Override
	public String toString() {
		if( this.getNumVal() == 1 || (this.getNumVal() == 10 && 
					!(this.getVal().equals("Ten"))) ) {
			return "" + this.getVal().charAt(0) + this.getSuit();
		} else {
			return "" + this.getNumVal() + this.getSuit();
		}
	}


}
