import java.util.*; //Scanner and ArrayLists
public class Blackjack {
	//Constant arrays used to create cards for new shoes
	public static final String[] SUITS = {"♣","♦","♥","♠"};
	public static final String[] VALUES = {"Ace", "King", "Queen", "Jack", "Ten",
		"Nine", "Eight", "Seven", "Six", "Five", "Four", "Three", "Two"};
	public static final int MIN_BET = 5;
	public static final int MAX_BET = 1000;
	public static final int CARDS_PER_DECK = 52;

	public static ArrayList<Card> deck;
	public static int numDecks;
	public static Player player;
	public static Dealer dealer;
	public static int cardsRemoved;

	public static void main(String[] args) {
		//Instantiate all class variables 
		Scanner scan = new Scanner(System.in);
		player = new Player();
		dealer = new Dealer();
		System.out.println("Welcome! Please specify how many decks you would like "+
				"to play with:");
		numDecks = scan.nextInt();
		deck = new ArrayList<Card>();
		newShoe();
		//Main game loop
		while(true) {
			//Set wager
			System.out.println("How much would you like to wager? Available balance: $" + player.getBankRoll());
			int bet = scan.nextInt();
			while (bet < MIN_BET || bet > MAX_BET || bet > player.getBankRoll()) {
				if(bet > player.getBankRoll()) {
					System.out.println("You do not have enough. Available balance: $" + player.getBankRoll());
				} else {
					System.out.println("Invalid wager. Please bet between $5 and $1000. Available balance: $" + player.getBankRoll());
				}
				bet = scan.nextInt();
			}
			player.bet(bet);
			
			System.out.println("Dealing cards...");
			//Deal 2 cards per player
			for(int i = 0; i < 2; i++) {
				//Deals cards by removing from beginning of ArrayList.
				dealCard(player);
				dealCard(dealer);
			}
			
			//TODO: Ask for insurance if dealer shows ACE
			
			//Player loop
			boolean playerContinue = true, playerBust = false, playerBJ = false;
			while(playerContinue && !dealer.getHand().blackJack()) {
				//first check to see if player gets 21
				if(player.getHand().blackJack()) {
					System.out.println("Blackjack! Your hand: " + player.getHand().toString());
					playerBJ = true;
					playerContinue = false;
				} else {
					//Let player make their decision
					System.out.println("Dealer showing: " + dealer.showing() + " Your hand: " + player.getHand().toString() + " Total: " + player.getHand().getTotal());
					System.out.println("What would you like to do? (H)it, (S)tand, (D)ouble Down, Consult the (B)ook?");
					String input = scan.next();
					char decision = input.charAt(0); //Only take first letter of input to avoid having to check whole strings
					if(decision == 'h' || decision == 'H') {
						dealCard(player);
					} else if(decision == 's' || decision == 'S') {
						playerContinue = false;
					} else if(decision == 'd' || decision == 'D') {
						player.doubleDown();
						dealCard(player);
						playerContinue = false;
						//Show player hand after double down because it will not loop back
						System.out.println("Your hand: " + player.getHand().toString() + " Total: " + player.getHand().getTotal());
					} else {
						System.out.println("Invalid input: " + decision);
					}
					//Automatically end looping once player busts
					if(player.getHand().getTotal() > 21) {
						System.out.println("Your hand: " + player.getHand().toString() + " Total: " + player.getHand().getTotal() + ". BUST!");
						playerContinue = false;
						playerBust = true;
					}
				}
			}
			
			//Condition for player having blackjack and dealer showing an ace.
			if(playerBJ && dealer.showingAce()) {
				System.out.println("Even money? (y)es or (n)o.");
				char decision = scan.next().charAt(0);
				if(decision == 'y' || decision == 'Y') {
					player.win();
				}
			}
			
			//Dealer loop
			//Show dealer's first two cards
			System.out.println("Dealer hand: " + dealer.getHand().toString() + " Total: " + dealer.getHand().getTotal());
			boolean dealerBust = false;
			//Loop until dealer reaches 
			while(dealer.getHand().getTotal() < 17 && !playerBust) {
				dealCard(dealer);
				System.out.println("Dealer hand: " + dealer.getHand().toString() + " Total: " + dealer.getHand().getTotal());
			}
			
			if(dealer.getHand().getTotal() > 21)
				dealerBust = true;
			
			if(playerBust) {
				System.out.println("You busted. You lose.");
				player.lose();
			} else if(dealerBust) {
				System.out.println("Dealer busts! You win!");
				player.win();
			} else if(player.getHand().getTotal() > dealer.getHand().getTotal()) {
				System.out.println("You beat the dealer! You win!");
				player.win();
			} else if(player.getHand().getTotal() == dealer.getHand().getTotal()) {
				System.out.println("Push");
				player.push();
			} else {
				System.out.println("The dealer beat you. You lose.");
				player.lose();
			}
			player.clearHand();
			dealer.clearHand();
		}
		
	}

	public static void newShoe() {
		deck.clear();
		//Generate new cards to be added to the shoe
		for(int i = 0; i < SUITS.length; i++) {
			for(int j = 0; j < VALUES.length; j++) {
				for(int k = 0; k < numDecks; k++) {
					deck.add(new Card(VALUES[j],SUITS[i]));
				}
			}	
		}

		//Shuffle the shoe
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));
	}
	
	public static void dealCard(Person p) {
		cardsRemoved++;
		p.deal(deck.remove(0));
	}
	
	public static void dealerLoop() {
		
	}
	
	public static void endGame(boolean playerBust, boolean dealerBust) {
		if(playerBust) {
			System.out.println("You busted. You lose.");
			player.lose();
		} else if(dealerBust) {
			System.out.println("Dealer busts! You win!");
			player.win();
		} else if(player.getHand().getTotal() > dealer.getHand().getTotal()) {
			System.out.println("You beat the dealer! You win!");
			player.win();
		} else if(player.getHand().getTotal() == dealer.getHand().getTotal()) {
			System.out.println("Push");
			player.push();
		} else {
			System.out.println("The dealer beat you. You lose.");
			player.lose();
		}
	}
}
