public class Person {
	private BJHand hand;
	
	public Person() {
		hand = new BJHand();
	}
	
	public void initHand() {
		hand = new BJHand();
	}
	
	public BJHand getHand() {
		return hand;
	}
	
	public void deal(Card c) {
		hand.dealCard(c);
	}
	
	public void clearHand() {
		hand.reset();
	}
}
