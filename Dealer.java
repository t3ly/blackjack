
public class Dealer extends Person {
	public String showing() {
		return super.getHand().getFirstCard().toString();
	}
	
	public boolean showingAce() {
		return showing().charAt(0) == 'A';
	}
}
