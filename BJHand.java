import java.util.*; //ArrayList
public class BJHand {
	public static final int SOFT_BOUND = 11; //Any hand less than 11 and with and ace is considered soft
	
	private ArrayList<Card> cards;
	private int total;
	private boolean sameCards, hasAce;
	
	//Constructor
	public BJHand() {
		cards = new ArrayList<Card>();
		setTotal(0);
		sameCards = false;
		hasAce = false;
	}
	
	private void setTotal(int tot) {
		total = tot;
	}

	public void dealCard(Card c) {
		cards.add(c);
		update();
	}

	public void update() {
		//Checks to see if the cards are the same and is available to split
		if(cards.size() == 2 && cards.get(0).equals(cards.get(1))) {
			sameCards = true;
		} else {
			sameCards = false;
		}
		
		int sum = 0;
		for(int i = 0; i < cards.size(); i++) {
			Card temp = cards.get(i);
			if(temp.getVal().equals("Ace")) {
				hasAce = true;
			}
			sum += cards.get(i).getNumVal();
		}
		
		if(hasAce && sum <= SOFT_BOUND) {
			sum += 10;
		}
		
		setTotal(sum);
	}
	
	public int getTotal() {
		return total;
	}
	
	public boolean canSplit() {
		return sameCards;
	}
	
	public Card getFirstCard() {
		return cards.get(0);
	}

	public void reset() {
		cards.clear();
		sameCards = false;
		hasAce = false;

	}
	
	public boolean blackJack() {
		return (cards.size() == 2 && this.getTotal() == 21);
	}
	
	@Override
	public String toString() {
		String toReturn = cards.get(0).toString();
		for(int i = 1; i < cards.size(); i++) {
			toReturn += ", " + cards.get(i).toString();
		}
		return toReturn;
	}
}
